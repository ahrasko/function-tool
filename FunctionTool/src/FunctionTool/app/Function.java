package FunctionTool.app;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Andrej Hraško
 */
public class Function {
 
    private String name;
    private String text;
    private Integer id;
    private String type;
    private String note;

    public Function(String name, String text) {
        this.name = name;
        this.text = text;
        this.type = "General";
        this.note = "";
    }

    public Function() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId() {
        return id;
    }
    
    public void setId(Integer Id) {
        this.id = Id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
    
}
