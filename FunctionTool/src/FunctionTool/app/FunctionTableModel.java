/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Class used to create and fill the table of functions. Modification will
 * change the look of the table.
 * Cell with name of the function is editable and will change the name in DB.
 * 
 * @author Andrej Hraško
 */
public class FunctionTableModel extends AbstractTableModel{
    
    FunctionManager functionManager = new FunctionManager();
    List<Function> functions = functionManager.getAllFunctions();
    

    @Override
    public int getRowCount() {
        return functions.size();
    }

    @Override
    public int getColumnCount() {
        //this will display ID in the function table. Just uncomment everything with ☼ and fix cases
        //return 2;
        return 1;
    }
    
    @Override
    public  Object getValueAt(int rowIndex, int columnIndex) {
        Function function = functions.get(rowIndex);
        switch (columnIndex) {
            //☼
            //case 0:
              //  return function.getId();
            case 0:
                return function.getName();
            
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Function function = functions.get(rowIndex);
        switch (columnIndex) {
            //☼
            //case 0:
                //function.setId((Integer) aValue);
            case 0:
                function.setName((String) aValue);
                break;            
            default:
                throw new IllegalArgumentException("columnIndex");
        }
        functionManager.editFunction(function);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            //☼
            //case 0:
             //   return "ID";
            case 0:
                return "Name";
             
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
           //☼
            // case 0:
            //    return Long.class;
            case 0:
                return String.class;
          
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            //☼
            //case 0:
                //return false;
            case 0:
                return true;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    /**
     * Methods used to manage functions in created table.
     * The table will react on changes in real time.
     */
    
    public Function returnFunction(int rowIndex) {
        Function func = functions.get(rowIndex);
        return func;
    }
    
    public void addFunction(Function function) {
        functions.add(function);
        
        int lastRow = functions.size() - 1;
        fireTableRowsInserted(lastRow, lastRow);
        fireTableDataChanged();
    }
    
    public void removeFunction(int rowIndex){
        Function temp = functions.get(rowIndex);
        functionManager.removeFunction(temp);
        functions.remove(temp);
        
        int lastRow = functions.size() - 1;
        fireTableRowsDeleted(lastRow, lastRow);
    }
}
