/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

/**
 *
 * @author Andrej Hraško
 */
public class Relation {
    int fId;
    int aId;
    String aName;

    public Relation(){
        
    }
    
    public Relation(int fId, String aName) {
        this.fId = fId;
        this.aName = aName;
    }

    public int getAId() {
        return aId;
    }

    public void setAId(int aId) {
        this.aId = aId;
    }

    public int getFId() {
        return fId;
    }

    public void setFId(int fId) {
        this.fId = fId;
    }

    public String getAName() {
        return aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Relation other = (Relation) obj;
        if (this.fId != other.fId) {
            return false;
        }
        if ((this.aName == null) ? (other.aName != null) : !this.aName.equals(other.aName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.fId;
        hash = 29 * hash + (this.aName != null ? this.aName.hashCode() : 0);
        return hash;
    }
    
    
}
