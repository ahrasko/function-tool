/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import javax.swing.*;
import java.awt.*;
import javax.imageio.*;
import java.io.*;

/**
 * jPanel used for displaying the MathML image of given function.
 * 
 * @author Andrej Hraško
 */
public class ImagePanel extends JPanel {

    //path of image
    private String path;
    //image object
    Image img;

    public ImagePanel() {
        Thread repainter = new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) { // I recommend setting a condition for your panel being open/visible
                    repaint();
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        });
        repainter.setName("Panel repaint");
        repainter.setPriority(Thread.MIN_PRIORITY);
        repainter.start();

        try {
            //save path
            path = "Example5.png";
            //load image
            img = ImageIO.read(new File(path));
        } catch (IOException ex) {
        }
    }

    ImagePanel(Image img) {
        this.img = img;
        
    }

    public void setImg(Image img) {
        this.img = img;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            img = ImageIO.read(new File(path));
        }catch(IOException ex){
            
        }
        // Draw image centered in the middle of the panel
        g.drawImage(img, 0, 0, this);
    }

    
}