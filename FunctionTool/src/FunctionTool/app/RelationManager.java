/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrej Hraško
 */
public class RelationManager {

    AtributeManager atributeManager = new AtributeManager();

    public void initTables() {
        Connection conn;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:relations.db");
            Statement stat = conn.createStatement();
            //stat.executeUpdate("drop table if exists functions;");
            stat.executeUpdate("create table if not exists relations (fid, aname);");

        } catch (SQLException ex) {
            //logger.error("Init tables sql exception ", ex);
        } catch (ClassNotFoundException ex) {
            //logger.error("Init tables ClassNotFoundException", ex);
        } finally {
        }
    }

    /**
     * Inserts new relation into database.
     * 
     * @param relation New Relation object about to be inserted.
     */
    public void insertRelation(Relation relation) {

        Connection conn = null;
        PreparedStatement stat = null;

        if (!isInDatabase(relation)) {
            try {
                conn = DriverManager.getConnection("jdbc:sqlite:relations.db");

                stat = conn.prepareStatement("INSERT INTO relations (fid, aname) VALUES (?, ?)");
                stat.setInt(1, relation.getFId());
                stat.setString(2, relation.getAName());
                stat.addBatch();

                conn.setAutoCommit(false);
                stat.executeBatch();
                conn.setAutoCommit(true);

                //System.out.println("rel: " +relation.getFId() +"-" +relation.getAName());
                
            } catch (SQLException ex) {
                //logger.error("Insert User SQL Exception", ex);
            } finally {
                closeStatement(stat);
                closeConnection(conn);
            }
        }
    }

    public void insertListOfRelations(List<Relation> relations) {
        for (int i = 0; i < relations.size(); i++) {
            insertRelation(relations.get(i));
        }
    }

    /**
     * Removes relation from database.
     * 
     * @param relation Relation about to be removed.
     */
    public void removeRelation(Relation relation) {
        if (relation == null) {
            throw new NullPointerException("Relation is null");
        }
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.prepareStatement("DELETE FROM relations WHERE fid=? AND aname=?");
            statement.setInt(1, relation.getFId());
            statement.setString(2, relation.getAName());
            statement.execute();

        } catch (SQLException ex) {
            //logger.error("Delete User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }

    /**
     * Removes all relations of given function from database.
     * 
     * @param function Function wich relations we want to remove.
     */
    public void removeFunctionRelations(Function function) {

        if (function == null) {
            throw new NullPointerException("Function is null");
        }
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.prepareStatement("DELETE FROM relations WHERE fid=?");
            statement.setInt(1, function.getId());
            statement.execute();

        } catch (SQLException ex) {
            //logger.error("Delete User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }


    /**
     * Returns list of all relations in database.
     * This method is used for table initialization at the start of the aplication
     * and for exporting to the sbml file.
     * 
     * @return List of all relations.
     */
    public List<Relation> getAllRelations() {
        List<Relation> relations = new ArrayList<Relation>();
        Connection connection = null;
        Statement statement = null;

        // if(ds==null) throw new NullPointerException("data source is null");

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM relations;");

            while (rs.next()) {
                Relation newRelation = new Relation();
                newRelation.setFId(rs.getInt(1));
                newRelation.setAName(rs.getString(2));

                relations.add(newRelation);
            }

        } catch (SQLException ex) {
            //logger.error("Get All Users SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
        return relations;
    }

    /**
     * Returns list of all relations of given function in database.
     * 
     * @return List of relations.
     */
    public List<Relation> getAllRelationsOfFunction(Function function) {
        List<Relation> relations = new ArrayList<Relation>();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.prepareStatement("SELECT * FROM relations WHERE fid=?");
            statement.setInt(1, function.getId());

            boolean result = statement.execute();
            if (result) {
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                    Relation newRelation = new Relation();
                    newRelation.setFId(rs.getInt(1));
                    newRelation.setAName(rs.getString(2));
                    relations.add(newRelation);
                }
            } else {
                System.out.println("No such relation with asked name.");
            }

        } catch (SQLException ex) {
            //logger.error("Get User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);

            return relations;
        }
    }

    /** 
     * Returns list of all relations of given atribute in database.
     * 
     * @return List of relations.
     */
    public List<Relation> getAllRelationsOfAtribute(Atribute atribute) {
        List<Relation> relations = new ArrayList<Relation>();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.prepareStatement("SELECT * FROM relations WHERE aname=?");
            statement.setString(1, atribute.getName());

            boolean result = statement.execute();
            if (result) {
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                    Relation newRelation = new Relation();
                    newRelation.setFId(rs.getInt(1));
                    newRelation.setAName(rs.getString(2));
                    relations.add(newRelation);
                }
            } else {
                System.out.println("No such relation with asked name.");
            }

        } catch (SQLException ex) {
            //logger.error("Get User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);

            return relations;
        }
    }

    
    public boolean isInDatabaseJustOnce(Atribute atribute) {
        List<Relation> tempList = getAllRelationsOfAtribute(atribute);
        if (tempList.size() > 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * This method will check the database for given relation.
     * 
     * @param relation Relation we are looking for.
     * @return true if given relation is in database
     */
    public boolean isInDatabase(Relation relation) {

        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:relations.db");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM relations;");
            Relation newRelation = new Relation();

            while (rs.next()) {

                newRelation.setFId(rs.getInt(1));
                newRelation.setAName(rs.getString(2));
                if (newRelation.equals(relation)) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            //logger.error("Get All Users SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
        return false;
    }

    /**
     * Statement closing.
     * @param statement 
     */
    public static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                //logger.error("Close Statement SQL Exception", ex);
            }
        }
    }

    /**
     * Connection closing,
     * @param connection 
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                //logger.error("Close Connection SQL Exception", ex);
            }
        }
    }
}
