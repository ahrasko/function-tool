package FunctionTool.app;

import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.tree.TreeNode;
import org.sbml.jsbml.*;
import org.sbml.jsbml.util.TreeNodeChangeListener;

/**
 * Creates an {@link SBMLDocument} and writes it’s content to disk.
 * 
 * @author Andrej Hraško
 */
public class Export implements TreeNodeChangeListener {

    /**
     * Exports given list of functions into SBML file.
     * 
     * @param functionList List of functions to export.
     * @param path Path and name of the created file.
     * @throws Exception 
     */
    public Export(List<Function> functionList, List<Atribute> atributeList, String path) throws Exception {
        // Create a new SBMLDocument, using SBML level 2 version 4.
        SBMLDocument doc = new SBMLDocument(2, 4);
        doc.addTreeNodeChangeListener(this);

        // Create a new SBML-Model and compartment in the document
        Model model = doc.createModel("test_model");
        Compartment compartment = model.createCompartment("default");
        compartment.setSize(1d);

        // Create model history
        History hist = new History();
        Creator creator = new Creator("Default First Name", "Default Family Name",
                "Default Organisation", "Default@EMail.com");
        hist.addCreator(creator);
        model.setHistory(hist);

        //moje
        
        for (int i = 0; i < functionList.size(); i++) {
            Function newFunction = functionList.get(i);
            
            FunctionDefinition f = model.createFunctionDefinition();//mozem aj vytvorit a potom pridat (new -> add)
            f.setName(newFunction.getName());
            f.setMath(JSBML.parseFormula("lambda(" + newFunction.getText() + ")"));
            f.setNotes(newFunction.getNote() + "/fT:" + newFunction.getType());
            //System.out.println(f.getArgument(1).getName()); 
        }
        
        for (int j = 0; j < atributeList.size(); j++) {
            Atribute newAtribute = atributeList.get(j);
            Unit unit = new Unit();
            if (newAtribute.getType().equals("length")) {
                if (newAtribute.getKind().equals("metre")) {
                    unit.setKind(Unit.Kind.METRE);
                }
                if (newAtribute.getKind().equals("dimensionless")) {
                    unit.setKind(Unit.Kind.DIMENSIONLESS);
                }
            }
            if (newAtribute.getType().equals("substance")) {
                if (newAtribute.getKind().equals("mole")) {
                    unit.setKind(Unit.Kind.MOLE);
                }
                if (newAtribute.getKind().equals("gram")) {
                    unit.setKind(Unit.Kind.GRAM);
                }
                if (newAtribute.getKind().equals("kilogram")) {
                    unit.setKind(Unit.Kind.GRAM);
                    unit.setScale(3);
                }
                if(newAtribute.getKind().equals("dimensionless"))unit.setKind(Unit.Kind.DIMENSIONLESS);
            }
            if (newAtribute.getType().equals("time")) {
                if (newAtribute.getKind().equals("second")) {
                    unit.setKind(Unit.Kind.SECOND);
                }
                if(newAtribute.getKind().equals("dimensionless"))unit.setKind(Unit.Kind.DIMENSIONLESS);
            }
            if (newAtribute.getType().equals("volume")) {
                if (newAtribute.getKind().equals("litre")) {
                    unit.setKind(Unit.Kind.LITRE);
                }
                if (newAtribute.getKind().equals("cubic metre")) {
                    unit.setKind(Unit.Kind.LITRE);
                    unit.setScale(-3);
                }
                if(newAtribute.getKind().equals("dimensionless"))unit.setKind(Unit.Kind.DIMENSIONLESS);
            }
            ListOf<Unit> listOfUnits = new ListOf<Unit>();
            listOfUnits.add(unit);
            
            UnitDefinition unitModel = model.createUnitDefinition();
            unitModel.setId(newAtribute.getName());
            unitModel.setListOfUnits(listOfUnits);

/*
            if(newAtribute.getType().equals("area")) u = UnitDefinition.area(2, 4);
            if(newAtribute.getType().equals("length")) u = UnitDefinition.length(2, 4);
            if(newAtribute.getType().equals("substance")) u = UnitDefinition.substance(2, 4);
            if(newAtribute.getType().equals("time")) u = UnitDefinition.time(2, 4);
            if(newAtribute.getType().equals("volume")) u = UnitDefinition.volume(2, 4);
*/             
        }

        // Write the SBML document to disk
        SBMLWriter.write(doc, path+".sbml.xml", "ProgName", "Version");
        //SBMLWriter.write(doc, System.out, ' ', (short) 2);
    }


    /* Those three methods respond to events from SBaseChangedListener */
    @Override
    public void nodeAdded(TreeNode sb) {
        System.out.println("[ADD] " + sb);
    }
    
    @Override
    public void nodeRemoved(TreeNode sb) {
        System.out.println("[RMV] " + sb);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent ev) {
        System.out.println("[CHG] " + ev);
    }
}
