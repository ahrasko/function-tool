/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

/**
 *
 * @author Andrej Hraško
 */
public class Atribute {
    
    private String name;
    private Integer id;
    //volume, substance, time
    private String type;
    /*
     * volume = {litre, kubic metre, dimensionless}, litre default;
     * substance = {mole, item, gram, kilogram, dimensionless}, mole default;
     * time = {second, dimensionless}, second default;
     */
    private String kind;
    
    public Atribute(){
        
    }
    
    public Atribute(String name){
        this.name=name;
        this.type="";
        this.kind="";
    }
    
    public Atribute(String name, String type){
        this.name=name;
        this.type=type;
        if(type.equals("volume")) this.kind="litre";
        if(type.equals("substance")) this.kind="mole";
        if(type.equals("time")) this.kind="second";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKind() {
        return kind;
    }

    /**
     * This sets kind of atribute. Only certain values are allowed and depend on the type.
     * @param kind 
     */
    public void setKind(String kind) {
        if(type.equals("substance")){
            if(kind.equals("mole")||kind.equals("gram")||kind.equals("kilogram")||kind.equals("dimensionless")||kind.equals("item")||kind.equals("")){
                this.kind = kind;
            }else{
                throw new IllegalArgumentException("Only mole, gram, kilogram, item or dimensionless allowed.");
            }
        }
        if(type.equals("volume")){
            if(kind.equals("litre")||kind.equals("cubic metre")||kind.equals("dimensionless")||kind.equals("")){
                this.kind = kind;
            }else{
                throw new IllegalArgumentException("Only litre, cubic metre or dimensionless allowed.");
            }
        }
        if(type.equals("time")){
            if(kind.equals("second")||kind.equals("dimensionless")||kind.equals("")){
                this.kind = kind;
            }else{
                throw new IllegalArgumentException("Only second or dimensionless allowed.");
            }
        }
        if(type.equals("area")){
            if(kind.equals("square metre")||kind.equals("dimensionless")||kind.equals("")){
                this.kind = kind;
            }else{
                throw new IllegalArgumentException("Only square metre or dimensionless allowed.");
            }
        }
        if(type.equals("length")){
            if(kind.equals("metre")||kind.equals("dimensionless")||kind.equals("")){
                this.kind = kind;
            }else{
                throw new IllegalArgumentException("Only metre or dimensionless allowed.");
            }
        }
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type.equals("length")) {
            this.type = type;
            this.kind = "metre";
        }
        if (type.equals("area")) {
            this.type = type;
            this.kind = "square metre";
        }
        if (type.equals("volume")) {
            this.type = type;
            this.kind = "litre";
        }
        if (type.equals("time")) {
            this.type = type;
            this.kind = "second";
        }
        if (type.equals("")) {
            this.type = type;
        }
        if (type.equals("substance")) {
            this.type = type;
            this.kind = "mole";
        }
    }

    /*@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Atribute other = (Atribute) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.type == null) ? (other.type != null) : !this.type.equals(other.type)) {
            return false;
        }
        if ((this.kind == null) ? (other.kind != null) : !this.kind.equals(other.kind)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 47 * hash + (this.kind != null ? this.kind.hashCode() : 0);
        return hash;
    }
   */

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Atribute other = (Atribute) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
    
    
}
