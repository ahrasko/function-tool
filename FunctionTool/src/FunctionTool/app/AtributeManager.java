/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrej Hraško
 */
public class AtributeManager {

    RelationManager relationManager;
    
    public void initTables() {
        Connection conn;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            Statement stat = conn.createStatement();
            //stat.executeUpdate("drop table if exists functions;");
            stat.executeUpdate("create table if not exists atributes (id INTEGER PRIMARY KEY AUTOINCREMENT, name, type, kind);");

        } catch (SQLException ex) {
            //logger.error("Init tables sql exception ", ex);
        } catch (ClassNotFoundException ex) {
            //logger.error("Init tables ClassNotFoundException", ex);
        } finally {
        }
    }

    /**
     * Inserts new function into database.
     * 
     * @param newAtribute New Function object about to be inserted.
     */
    public void insertAtribute(Atribute newAtribute) {

        if (newAtribute.getName() == null) {
            throw new NullPointerException("atribute name is null");
        }

        if (newAtribute.getType() == null) {
            throw new NullPointerException("atribute text is null");
        }

        /*if (newAtribute.getId() != null) {
            throw new IllegalArgumentException("atribute has already assigned id");
        }*/
        Connection conn = null;
        PreparedStatement stat = null;

        if (!isInDatabase(newAtribute)) {
            try {
                conn = DriverManager.getConnection("jdbc:sqlite:atributes.db");

                stat = conn.prepareStatement("INSERT INTO atributes (name, type, kind) VALUES (?, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                stat.setString(1, newAtribute.getName());
                stat.setString(2, newAtribute.getType());
                stat.setString(3, newAtribute.getKind());
                stat.addBatch();

                conn.setAutoCommit(false);
                stat.executeBatch();
                conn.setAutoCommit(true);

                int id = getId(stat.getGeneratedKeys());
                newAtribute.setId(id);
                //System.out.println("atr:"+id +"-"+newAtribute.getName());

            } catch (SQLException ex) {
                //logger.error("Insert User SQL Exception", ex);
            } finally {
                closeStatement(stat);
                closeConnection(conn);
            }
        }
    }

    public void inserListOfAtributes(List<Atribute> atributes) {
        for (int i = 0; i < atributes.size(); i++) {
            insertAtribute(atributes.get(i));
        }
    }

    /**
     * Removes atribute from database.
     * 
     * @param atribute Atribute about to be removed.
     */
    public void removeAtribute(Atribute atribute) {
        if (atribute == null) {
            throw new NullPointerException("Atribute is null");
        }
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            statement = connection.prepareStatement("DELETE FROM atributes WHERE id=?");
            statement.setInt(1, atribute.getId());
            statement.execute();

        } catch (SQLException ex) {
            //logger.error("Delete User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }

    /**
     * Method for editing atributes. It finds the atribute with same ID 
     * in DB and change name,text or type.
     * 
     * @param atribute Changed atribute with valid ID.
     */
    public void editAtribute(Atribute atribute) {


        if (atribute.getName() == null) {
            throw new NullPointerException("atribute name is null");
        }

        if (atribute.getType() == null) {
            throw new NullPointerException("atribute text is null");
        }

        if (atribute.getId() == null) {
            throw new NullPointerException("atribute id is null");
        }

        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            stat = conn.prepareStatement("UPDATE atributes SET name=?, type=?, kind=? WHERE id=?");
            stat.setString(1, atribute.getName());
            stat.setString(2, atribute.getType());
            stat.setString(3, atribute.getKind());
            stat.setInt(4, atribute.getId());

            stat.execute();

        } catch (SQLException ex) {
            //logger.error("Edit Record SQL Exception", ex);
        } finally {
            closeStatement(stat);
            closeConnection(conn);
        }
    }

    /**
     * Returns atribute with given name.
     * 
     * @param name Name of the atribute.
     * @return Atribute from database.
     */
    public Atribute getAtribute(String name) {
        Atribute atribute = new Atribute();
        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            statement = connection.prepareStatement("SELECT * FROM Atributes WHERE name=?");
            statement.setString(1, name);

            boolean result = statement.execute();
            if (result) {
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                    atribute.setId(rs.getInt(1));
                    atribute.setName(rs.getString(2));
                    atribute.setType(rs.getString(3));
                    atribute.setKind(rs.getString(4));
                }
            } else {
                System.out.println("No such atribute with asked name.");
            }

        } catch (SQLException ex) {
            //logger.error("Get User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);

            return atribute;
        }
    }

    /**
     * Returns list of all atributes in database.
     * This method is used for table initialization at the start of the aplication
     * and for exporting to the sbml file.
     * 
     * @return List of all atributes.
     */
    public List<Atribute> getAllAtributes() {
        List<Atribute> atributes = new ArrayList<Atribute>();
        Connection connection = null;
        Statement statement = null;

        // if(ds==null) throw new NullPointerException("data source is null");

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM atributes;");

            while (rs.next()) {
                Atribute newAtribute = new Atribute();
                newAtribute.setId(rs.getInt(1));
                newAtribute.setName(rs.getString(2));
                newAtribute.setType(rs.getString(3));
                newAtribute.setKind(rs.getString(4));
                atributes.add(newAtribute);
            }

        } catch (SQLException ex) {
            //logger.error("Get All Users SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
        return atributes;
    }
    
    /**
     * This method will check the database for given atribute.
     * 
     * @param atribute Atribute we are looking for.
     * @return true if given atribute is in database
     */
    public boolean isInDatabase(Atribute atribute) {

        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection("jdbc:sqlite:atributes.db");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM atributes;");
            Atribute newAtribute = new Atribute();
            
            while (rs.next()) {
                
                newAtribute.setName(rs.getString(2));
                newAtribute.setType(rs.getString(3));
                newAtribute.setKind(rs.getString(4));
                if (newAtribute.equals(atribute)) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            //logger.error("Get All Users SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
        return false;
    }

    /**
     * Method used for assign ID to new atribute. 
     * Used in insertAtribute(Atribute)  
     * 
     * @param keys Generated key from preparedStatement
     * @return key for the atribute.
     * @throws SQLException 
     */
    public int getId(ResultSet keys) throws SQLException {

        if (keys.getMetaData().getColumnCount() != 1) {
            //logError("Resultset in getId() had more columns.");
            throw new IllegalArgumentException("resultset has more columns");
        }

        if (keys.next()) {
            int Id = keys.getInt(1);
            if (keys.next()) {
                //logError("Resultset in getId() had more than one row.");
                throw new IllegalArgumentException("resultset has more rows");
            } else {
                return Id;
            }

        } else {
            //logError("Resultset in getId() had no rows.");
            throw new IllegalArgumentException("resultset has no rows");

        }
    }

    /**
     * Statement closing.
     * @param statement 
     */
    public static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                //logger.error("Close Statement SQL Exception", ex);
            }
        }
    }

    /**
     * Connection closing,
     * @param connection 
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                //logger.error("Close Connection SQL Exception", ex);
            }
        }
    }
}
