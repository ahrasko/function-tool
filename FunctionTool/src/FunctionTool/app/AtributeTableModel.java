/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 * Class used to create and fill the table of atributes. Modification will
 * change the look of the table.
 * 
 * @author Andrej Hraško
 */
public class AtributeTableModel extends AbstractTableModel{
    
    //FunctionManager functionManager = new FunctionManager();
    AtributeManager atributeManager = new AtributeManager();
    List<Atribute> atributes = atributeManager.getAllAtributes();
    
    
    @Override
    public int getRowCount() {
        return atributes.size();
    }

    @Override
    public int getColumnCount() {        
        return 3;
    }
    
    @Override
    public  Object getValueAt(int rowIndex, int columnIndex) {
        Atribute atribute = atributes.get(rowIndex);
        switch (columnIndex) {
 
            case 0:
                return atribute.getName();
            case 1:
                return atribute.getType();
            case 2:
                return atribute.getKind();
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        Atribute atribute = atributes.get(rowIndex);
        switch (columnIndex) {
            //☼
            //case 0:
                //function.setId((Integer) aValue);
            case 0:
                atribute.setName((String) aValue);
                break;       
            case 1:
                atribute.setType((String) aValue);
                break;
            case 2:
                atribute.setKind((String) aValue);
                break;    
            default:
                throw new IllegalArgumentException("columnIndex");
        }
        atributeManager.editAtribute(atribute);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
     @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {

            case 0:
                return "Name";
            case 1:
                return "Type";
            case 2:
                return "Kind";
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
     
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {

            case 0:
                return String.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return false;
            case 1:
                return true;
            case 2:
                return true;    
            default:
                throw new IllegalArgumentException("columnIndex");
        }
    }
    
    
    /**
     * Methods used to manage tributes in created table.
     * The table will react on changes in real time.
     */
    
    public Atribute returnAtribute(int rowIndex) {
        Atribute atr = atributes.get(rowIndex);
        return atr;
    }
    
    /**
     * Adds atribute into the table.
     * @param atribute 
     */
    public void addAtribute(Atribute atribute) {
        if(!isInTable(atribute)){
            atributes.add(atribute);
            
            int lastRow = atributes.size() - 1;
            fireTableRowsInserted(lastRow, lastRow);
            fireTableDataChanged();
       }
    }
    
    /**
     * Adds list of atributes into the table.
     * @param atributes 
     */
    public void addListOfAtributes(List<Atribute> atributes){
        for (int i = 0; i < atributes.size(); i++) {
            addAtribute(atributes.get(i));
        }
    }
    
    /**
     * Removes atribute from database and table as well.
     * 
     * @param atribute 
     */
    public void removeAtribute(Atribute atribute){
        
        atributeManager.removeAtribute(atribute);
        atributes.remove(atribute);
        int lastRow = atributes.size() - 1;
        fireTableRowsDeleted(lastRow, lastRow);
    }
    
    /**
     * 
     * @param function 
     */
    public boolean isInTable(Atribute atribute){
        for(int i=0; i<atributes.size();i++){
            if(atributes.get(i).equals(atribute)) return true;
        }
        return false;
    }
}
