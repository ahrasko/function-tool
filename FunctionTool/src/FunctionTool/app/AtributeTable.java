/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import javax.swing.*;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

/**
 *
 * @author Jozko
 */
public class AtributeTable extends JPanel{
    private boolean DEBUG = false;
    
    public AtributeTable() {
        super(new GridLayout(1,0));
 
        JTable table = new JTable(new AtributeTableModel());
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
 
        //Create the scroll pane and add the table to it.
        //JScrollPane scrollPane = new JScrollPane(table);
 
        //Set up column sizes.
        //initColumnSizes(table);
 
        //Fiddle with the Sport column's cell editors/renderers.
        setTypeColumn(table, table.getColumnModel().getColumn(2));
 
        //Add the scroll pane to this panel.
        //add(scrollPane);
    }
    
    public void setTypeColumn(JTable table,
                                 TableColumn sportColumn) {
        //Set up the editor for the sport cells.
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("substance");
        comboBox.addItem("volume");
        comboBox.addItem("time");
        
        sportColumn.setCellEditor(new DefaultCellEditor(comboBox));
 
        //Set up tool tips for the sport cells.
        DefaultTableCellRenderer renderer =
                new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        sportColumn.setCellRenderer(renderer);
    }
}
