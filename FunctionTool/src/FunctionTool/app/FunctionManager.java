package FunctionTool.app;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author Andrej Hraško
 */
public class FunctionManager {
    
    
    /**
     * Initializes the table of functions.
     */
    public void initTables() {   
        Connection conn;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:functions.db");
            Statement stat = conn.createStatement();
            //stat.executeUpdate("drop table if exists functions;");
            stat.executeUpdate("create table if not exists functions (id INTEGER PRIMARY KEY AUTOINCREMENT, name, text, type, note);");
            
        } catch (SQLException ex) {
            //logger.error("Init tables sql exception ", ex);
        } catch (ClassNotFoundException ex){
            //logger.error("Init tables ClassNotFoundException", ex);
        }finally {  
            
        }
    }
    
    
    /**
     * Method for editing functions. It finds the function with same ID 
     * in DB and change name,text or type.
     * 
     * @param function Changed function with valid ID.
     */
    public void editFunction(Function function) {
        
        
        if (function.getName() == null) {
            throw new NullPointerException("function name is null");
        }

        /*if (function.getText() == null) {
            throw new NullPointerException("function text is null");
        }*/

        if (function.getId() == null) {
            throw new NullPointerException("function id is null");
        }

        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:functions.db");
            stat = conn.prepareStatement("UPDATE functions SET name=?, text=?, type=?, note=? WHERE id=?");
            stat.setString(1, function.getName());
            stat.setString(2, function.getText());
            stat.setString(3, function.getType());
            stat.setString(4, function.getNote());
            stat.setInt(5, function.getId());
            
            stat.execute();
            
        } catch (SQLException ex) {
            //logger.error("Edit Record SQL Exception", ex);
        } finally {
            closeStatement(stat);
            closeConnection(conn);
        }
    }
    
    /**
     * Inserts new function into database.
     * 
     * @param newFunction New Function object about to be inserted.
     */
    public void insertFunction(Function newFunction) {
        
        if (newFunction.getName() == null) {
            throw new NullPointerException("function name is null");
        }
        
        /*if (newFunction.getText() == null) {
            throw new NullPointerException("function text is null");
        }*/
        
        if (newFunction.getId() != null) {
            throw new IllegalArgumentException("function has already assigned id");
        }
        Connection conn = null;
        PreparedStatement stat = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:functions.db");
            
            stat = conn.prepareStatement("INSERT INTO functions (name, text, type, note) VALUES (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            stat.setString(1, newFunction.getName());
            stat.setString(2, newFunction.getText());
            stat.setString(3, newFunction.getType());
            stat.setString(4, newFunction.getNote());
            stat.addBatch();
            
            conn.setAutoCommit(false);
            stat.executeBatch();
            conn.setAutoCommit(true);
            
            int id = getId(stat.getGeneratedKeys());
            newFunction.setId(id);
            
        } catch (SQLException ex) {
            //logger.error("Insert User SQL Exception", ex);
        } finally {
            closeStatement(stat);
            closeConnection(conn);
        }
    }
    
    /**
     * Returns function with given name.
     * 
     * @param name Name of the function.
     * @return Function from database.
     */
    public Function getFunction(String name) {
        Function function = new Function();
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:functions.db");
            statement = connection.prepareStatement("SELECT * FROM Functions WHERE name=?");
            statement.setString(1, name);
            
            boolean result = statement.execute();
            if (result) {
                ResultSet rs = statement.getResultSet();
                while (rs.next()) {
                    function.setId(rs.getInt(1));
                    function.setName(rs.getString(2));
                    function.setText(rs.getString(3));
                    function.setType(rs.getString(4));
                    function.setNote(rs.getString(5));
                }
            } else {
                System.out.println("No such function with asked name.");
            }

        } catch (SQLException ex) {
            //logger.error("Get User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);

            return function;
        }
    }
    
    /**
     * Removes function from database.
     * 
     * @param function Function about to be removed.
     */
    public void removeFunction(Function function) {
        if (function == null) {
            throw new NullPointerException("Function is null");
        }
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:functions.db");
            statement = connection.prepareStatement("DELETE FROM functions WHERE id=?");
            statement.setInt(1, function.getId());
            statement.execute();
            
        } catch (SQLException ex) {
            //logger.error("Delete User SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }
    
    /**
     * Returns list of all functions in database.
     * This method is used for table initialization at the start of the aplication
     * and for exporting to the sbml file.
     * 
     * @return List of all functions.
     */
    public List<Function> getAllFunctions() {
        List<Function> functions = new ArrayList<Function>();
        Connection connection = null;
        Statement statement = null;
        
       // if(ds==null) throw new NullPointerException("data source is null");
        
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:functions.db");
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM functions;");
            
            while (rs.next()) {
                Function newFunction = new Function();
                newFunction.setId(rs.getInt(1));
                newFunction.setName(rs.getString(2));
                newFunction.setText(rs.getString(3));
                newFunction.setType(rs.getString(4));
                newFunction.setNote(rs.getString(5));
                functions.add(newFunction);
            }
            
        } catch (SQLException ex) {
            //logger.error("Get All Users SQL Exception", ex);
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
        return functions;
    }
    
    /**
     * Returns list of atributes of the given function.
     * 
     * @param function Function type object.
     * @return List of atributes.
     */
    public List<Atribute> getAtributes(Function function){
        List<Atribute> atributes = new ArrayList<Atribute>();
        String fText = function.getText();
        String tempAtr ="";
        
        if(fText==null)return atributes;
        for (int i = 0; i < fText.length(); i++) {
            if(Character.isLetter(fText.charAt(i))){
                for(int j=i;j < fText.length(); j++){
                    char ch=fText.charAt(j);
                    if(!(ch=='+')&&!(ch=='-')&&!(ch=='*')&&!(ch=='/')&&!(ch=='.')&&!(ch=='^')&&!(ch=='(')&&!(ch==')')){
                        tempAtr=tempAtr+ch;
                        i++;
                    }else{
                        if(!(tempAtr.equals("sqrt")) && !(tempAtr.equals("sum")) && !(tempAtr.equals("lim")) && !(tempAtr.equals("infi"))){
                            atributes.add(new Atribute (tempAtr));
                            
                            tempAtr="";
                            i=j;
                            break;
                        }else{
                            tempAtr="";
                            i=j;
                            break;
                        }
                    }
                }
            }
        }
        if(!tempAtr.equals("")) atributes.add(new Atribute(tempAtr));
        
        return atributes;
    }
    
    /**
     * Method used for assign ID to new function. 
     * Used in insertFunction(Function)  
     * 
     * @param keys Generated key from preparedStatement
     * @return key for the function.
     * @throws SQLException 
     */
    public int getId(ResultSet keys) throws SQLException {

        if (keys.getMetaData().getColumnCount() != 1) {
            //logError("Resultset in getId() had more columns.");
            throw new IllegalArgumentException("resultset has more columns");
        }

        if (keys.next()) {
            int Id = keys.getInt(1);
            if (keys.next()) {
                //logError("Resultset in getId() had more than one row.");
                throw new IllegalArgumentException("resultset has more rows");
            } else {
                return Id;
            }

        } else {
            //logError("Resultset in getId() had no rows.");
            throw new IllegalArgumentException("resultset has no rows");

        }
    }

    /**
     * Statement closing.
     * @param statement 
     */
    public static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                //logger.error("Close Statement SQL Exception", ex);
            }
        }
    }

    /**
     * Connection closing,
     * @param connection 
     */
    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                //logger.error("Close Connection SQL Exception", ex);
            }
        }
    }
}
