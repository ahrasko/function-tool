/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.util.ArrayList;
import java.util.List;
import org.sbml.jsbml.*;

/**
 * Import from SBML file.
 * 
 * @author Andrej Hraško
 */
public class Import {    
    
    SBMLReader reader = new SBMLReader();
    SBMLDocument doc;
    ListOf<FunctionDefinition> listFD;
    ListOf<UnitDefinition> listUD;
    
    /**
     * Constructor will read the given file and copy the functions into the ListOf.
     * 
     * @param fileName Location and name of the file.
     * @throws Exception 
     */
    public Import(String fileName) throws Exception{
        doc = reader.readSBML(fileName+".sbml.xml");
        Model model = doc.getModel();
        listFD = model.getListOfFunctionDefinitions(); 
        listUD = model.getListOfUnitDefinitions();
    }
    
    /**
     * 
     * @return 
     */
    public List<Atribute> getAtributes(){
        List<Atribute> atributes = new ArrayList<Atribute>();
        ListOf<Unit> units;
        for(int i = 0;i<listUD.size();i++){
            Atribute newAtribute= new Atribute();
            units=listUD.get(i).getListOfUnits();
            
            newAtribute.setName(listUD.get(i).getId());
            
            if(units.get(0).getKind().equals(Unit.Kind.METRE)){
                    newAtribute.setType("length");
                    newAtribute.setKind("metre");
                }
            if(units.get(0).getKind().equals(Unit.Kind.MOLE)){
                    newAtribute.setType("substance");
                    newAtribute.setKind("mole");
                }
            if(units.get(0).getKind().equals(Unit.Kind.GRAM)){
                    newAtribute.setType("substance");
                    if(units.get(0).getScale()==3){
                        newAtribute.setKind("kilogram");
                    }else{
                        newAtribute.setKind("gram");
                    }
                }
            if(units.get(0).getKind().equals(Unit.Kind.SECOND)){
                    newAtribute.setType("time");
                    newAtribute.setKind("second");
                }
            if(units.get(0).getKind().equals(Unit.Kind.LITRE)){
                    newAtribute.setType("volume");
                    if(units.get(0).getScale()==-3){
                        newAtribute.setKind("cubic metre");
                    }else{
                        newAtribute.setKind("litre");
                    }
                }
            if(units.get(0).getKind().equals(Unit.Kind.DIMENSIONLESS)){
                    newAtribute.setType("");
                    newAtribute.setKind("");
                }
            atributes.add(newAtribute);
            
            //System.out.println(newAtribute.getName() +"-" +newAtribute.getType() +"-" +newAtribute.getKind());
        }
        
        return atributes;
    }
    
    /**
     * This method creates List<Function> from ListOf<FunctionDefinition> 
     * It is used for insertion to the DB.
     * 
     * @return List of Function type objects.  
     */
    public List<Function> getFunctions(){
        List<Function> functions = new ArrayList<Function>();
        for(int i = 0;i < listFD.size(); i++){
            Function newFunction = new Function();
            newFunction.setName(listFD.get(i).getName());
            newFunction.setText(listFD.get(i).getBody().toString());    
            newFunction.setNote(getNote(listFD.get(i).getNotesString()));
            newFunction.setType(getType(listFD.get(i).getNotesString()));
            
            functions.add(newFunction);
        }
        
        return functions; 
    }
    
    public String getNote(String input){
        String note="";
        int temp=0;
        
        for (int i = 0; i < input.length(); i++) {   
            if((input.charAt(i)=='<')&&(input.charAt(i+1)=='p')&&(input.charAt(i+2)=='>')){
                temp=i+3;
            }
            //return "";
        }
        for (int j=temp; j<input.length(); j++){
            if((input.charAt(j)=='/')&&(input.charAt(j+1)=='f')&&(input.charAt(j+2)=='T')) break;
            note=note+input.charAt(j);
        }
        return note;
    }
    
    public String getType(String input){
        String type="";
        int temp=0;
        
        for (int i = 0; i < input.length(); i++) {   
            if((input.charAt(i)=='/')&&(input.charAt(i+1)=='f')&&(input.charAt(i+2)=='T')){
                temp=i+4;
            }
            //return "General";
        }
        for (int j=temp; j<input.length(); j++){
            if((input.charAt(j)=='<')&&(input.charAt(j+1)=='/')&&(input.charAt(j+2)=='p')) break;
            type=type+input.charAt(j);
        }
        return type;
    }
}
