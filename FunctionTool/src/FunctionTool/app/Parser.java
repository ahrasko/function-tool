/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FunctionTool.app;

import java.awt.Color;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;

/**
 * Class for handling formulas. 
 * simple Tex -> Tex, then Tex -> PNG
 * 
 * @author Andrej Hraško
 */
public class Parser{
    

    /**
     * Creates .png image of the input text formula.
     * 
     * @param text Math formulaToTex written using LaTex commands.
     */
    public void textToPNG(String text) {
        if (text == null) {
            throw new NullPointerException("text je null!");
        }

        TeXFormula formula = new TeXFormula(text);
        formula.createPNG(TeXConstants.STYLE_DISPLAY, 13, "Example5.png", Color.lightGray, Color.black);

    }
    
    /**
     * This method ocnverts standard formula to formula with TeX representation of the fraction operator
     * It will change A/B input to the frac(A)(B), so jLaTeXMath have no problems to create PNG in textToPNG().  
     * 
     * @param text Standard mathematical formula.
     * @return Converted formula.
     */
    public String fracCreator(String text){
        String out="";
        String frac;
        String LS="";String PS="";
        int lomitko=0;
        int numL=0; int numP=0;
        
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            if(ch=='/') {
                lomitko = i;
                break;
            }
        }
        
        if(lomitko==0) return text;
        
        if(text.charAt(lomitko-1)==')'){
            LS=LS+")";
            int L=1; numL=1;
            int temp=lomitko-2;
            while(L>0){
                try{
                if(text.charAt(temp)=='('){
                    LS="("+LS;
                    L=L-1;
                }
                if(text.charAt(temp)==')'){
                    LS=")"+LS;
                    L=L+1;
                }   
                if(!(text.charAt(temp)==')')&& !(text.charAt(temp)=='(')){
                    LS=text.charAt(temp)+LS;
                }
                                
                numL++;
                temp--;    
                }catch (IndexOutOfBoundsException ex){
                    throw new ArrayIndexOutOfBoundsException("Error, check brackets!");
                }
            }
        }else {
            numL=1;
            int L=1;
            int temp=lomitko-1;
            if(!(Character.isDigit(text.charAt(temp))) && !(Character.isLetter(text.charAt(temp)))) 
                throw new IllegalArgumentException("Error in formula.Check operators or brackets");
            while(L>0){
                if((temp<0)||(!(Character.isDigit(text.charAt(temp))) && !(Character.isLetter(text.charAt(temp))))){
                    L=0;
                    numL--;
                } else {
                    LS=text.charAt(temp) +LS;
                    temp--;
                    numL++;
                }
            }
            LS= "(" +LS +")";
        }
        
        if(text.charAt(lomitko+1)=='('){
            PS=PS+"(";
            int P=1; numP=1;
            int temp=lomitko+2;
            while(P>0){
                try{
                if(text.charAt(temp)==')'){ 
                    PS=PS+")";
                    P--;
                }
                if(text.charAt(temp)=='('){ 
                    PS=PS+"(";
                    P++;
                }
                if(!(text.charAt(temp)==')')&& !(text.charAt(temp)=='(')){
                    PS=PS+text.charAt(temp);
                }
                numP++;
                temp++;
                }catch (IndexOutOfBoundsException ex){
                    throw new ArrayIndexOutOfBoundsException("Error, check brackets!");
                }
            }
            
        } else {
            numP=1;
            int P=1;
            int temp=lomitko+1;
            if(!(Character.isDigit(text.charAt(temp))) && !(Character.isLetter(text.charAt(temp)))) 
                throw new IllegalArgumentException("Error in formula.Check operators or brackets");
            while(P>0){
                if((temp>=text.length()) || (!(Character.isDigit(text.charAt(temp))) && !(Character.isLetter(text.charAt(temp))))){  
                    P=0;
                    numP--;
                } else {
                    PS=PS +text.charAt(temp);
                    numP++;
                    temp++;
                }
            }
            PS= "(" +PS +")";
        }
        
        frac="frac"+LS+PS;   
        
        for (int i = 0; i < lomitko-numL; i++) {
            char ch = text.charAt(i);
            out=out+ch;            
        }
        out=out+frac;

        for (int i = lomitko+numP+1; i < text.length(); i++) {
            char ch = text.charAt(i);
            out=out+ch;            
        }
        
        out=fracCreator(out);
        
        return out;
    }

    /**
     * Converts simplified Tex formulaToTex to full Tex formula.
     * 
     * @param text Math formulaToTex written in simplified LaTex commands.
     * @return Formula written in full LaTex.
     */
    public String textToTex(String text) {
        //pridat reakciu na prazdny  znak!...netreba.Vsak ak je prazdny, posunie sa dalej.
        if (text == null) {
            throw new NullPointerException("text je null!");
        }
        String s = "";

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (Character.isDigit(c)) {
                s += c;
            } else {

                if ((c == '.') || (c == '+') || (c == '-') || (c == '*') || (c == '^') || (c == '=') ||(c=='_')) {
                    s += c;
                }
                if ((c=='{') || (c=='}') || (c=='[') || (c==']') || (c=='\\')) s+=c;
                if (c == '(') {
                    s += '{';
                }
                if (c == ')') {
                    s += '}';
                }
                
                if (Character.isLetter(c)) {
                    int mark=1;
                    
                    //squared & sum
                    if ((c == 's') && (i+1<text.length()) && (text.charAt(i + 1) == 'q') && (text.charAt(i + 2) == 'r') && (text.charAt(i + 3) == 't') && (text.charAt(i + 4) == '(')) {
                        s += "\\sqrt";
                        i = i + 3;
                        mark--;
                    } 
                    if ((c == 's') && (i+1<text.length()) && (text.charAt(i + 1) == 'u') && (text.charAt(i + 2) == 'm') && (text.charAt(i + 3) == '(')) {
                        s += "\\sum_";
                        i = i + 2;
                        mark--;
                    } 
                    
                    //product
                    if ((c=='p') && (i+1<text.length()) && (text.charAt(i + 1) == 'r') && (text.charAt(i + 2) == 'o') && (text.charAt(i + 3) == 'd') && (text.charAt(i + 4) == '(')) {
                        s+="\\prod_";
                        i = i + 3;
                        mark--;
                    }
                    //fraction
                    if ((c == 'f') && (i+1<text.length()) && (text.charAt(i + 1) == 'r') && (text.charAt(i + 2) == 'a') && (text.charAt(i + 3) == 'c') && (text.charAt(i + 4) == '(')) {
                        s += "\\frac";
                        i = i + 3;
                        mark--;
                    }
                    
                    //limits ex:"lim(x\toinfi)x = lim (x→infinity)x"
                    if ((c == 'l') && (i+1<text.length()) && (text.charAt(i + 1) == 'i') && (text.charAt(i + 2) == 'm') && (text.charAt(i + 3) == '(')) {
                        s += "\\lim_";
                        i = i + 2;
                        mark--;
                    } 
                    
                    //infinity
                    if ((c == 'i') && (i+1<text.length()) && (text.charAt(i + 1) == 'n') && (text.charAt(i + 2) == 'f') && (text.charAt(i+3)=='i') && (text.charAt(i + 4) == '(')) {
                        s += "\\infty";
                        i = i + 3;
                        mark--;
                    }
                    
                    if(mark==1) s+=c;

                }
            }

        }
        System.out.println("S: " + s);
        return s;
    }

    /**
     * test metod. If it is possible to replace \frac by '/'
     * @param text Input formulaToTex.
     * @return changed text suitable for textToPNG(String)
     */
    
    public String formulaImpr(String text) {
        String H = "";
        if (text == null) {
            throw new NullPointerException("text je null!");
        }
        int l = 0;
        int p = 0;
        String t = "";

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (c == '(') {
                l++;
            }
            if (Character.isDigit(c)) {
                t += c;
            }
            if ((c == '.') || (c == '+') || (c == '-') || (c == '*') || (c == '^')) {
                t += c;
            }
            if ((c == ')') && (l > 0)) {
                t = "{" + t + "}";
                l--;
            }
            if ((c == ')') && (l == 0)) {
                H += "}";
            }
            if (c == '/') {
                if (l > 0) {
                    H = H + "{\\frac{" + t + "}";
                    t = "";
                } else {
                    H = H + "\\frac{" + t + "}";
                    t = "";
                }
            }
            if (Character.isLetter(c)) {
                if (!(c == 's')) {
                    t += c;
                }
                if ((c == 's') && !(text.charAt(i + 1) == 'q')) {
                    t += c;
                }
                if ((c == 's') && (text.charAt(i + 1) == 'q') && (text.charAt(i + 2) == 'r') && (text.charAt(i + 3) == 't') && (text.charAt(i + 4) == '(')) {
                    t += "\\sqrt";
                    i = i + 3;
                }

            }
        }
        H = H + t;
        System.out.println("H: " + H);
        System.out.println("T: " + t);
        return H;

    }
}
