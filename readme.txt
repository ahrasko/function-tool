FUNCTION TOOL

author: Andrej Hra�ko
mail:alucardomg@gmail.com
latest version: from 19.5.2012
Masaryk University Brno, Faculty of Informatics

Please email me if you find any bugs, issues or have anything to ask. 

-About-

This tool is part of my bachelor thesis and is created for the project e-photosynthesis.org as a simple offline editing tool for mathematical functions. It is written in Java language and uses various tools for managing .sbml data format for biological data, jsbml library for handling this format in java applications, jlatexmath for the vizualization 
of functions in MathML standard.
The whole project is written using NetBeans IDE with Swing GUI Builder.
At the end of this part I want to thank authors of JSBML, JLaTeXMath and Launch4j for creating tools which helped me a lot.

-NEWS-

> Import and Export now works. Bugs are very much possible, so please email me
  if you find some
> Saving and editing of atributes in database now work.
> Issue with image refreshing is now fixed.Last generated image is loaded at the
  start, but I think it is not so big deal.

-Organization of this file-

Java classes are in src/FunctionTool/app
FunctionTool.jar file is in directory dist. Together with auto-generated readme and library file.
Main file:
	-Example5.png is image which is used for MathML vizualization of 	 	 expressions
	-icon file
	-FunctionTool.exe executable file.
	-L1.xml, auto-generated log for creating .exe file using launch4j
	-build.xml, euto-generated log for builded project in NetBeans
	-export.sbml.xml, exported database used for test purpose
	-functions.db, database of functions
	-importTest.sbml.xml, file for testing imports 
	-jsbml.log, log for jsbml library

-INSTRUCTIONS-

You can change type and kind of atribute. See allowed values below:
|-TYPE-    | -KIND-                                    |
|substance | mole, gram, kilogram, item, dimensionless |
|volume    | litre, kubic metre, dimensionless         |
|time      | second, dimensionless                     |
|area      | square metre, dimensionless               | 
|length    | metre, dimensionless                      |

You can rename the function by doubleclick on the name in the table.

Choose function from table, press LOAD and the text will show in the right top field. It is editable.

ShowMathML now uses standard input (for ex. "sqrt(a+b/c)").

Export/Import:
When importing, you can enter the whole file path or put the file to the source directory and simply enter name of that file (for ex. import.sbml.xml)
You can also choose the name of exported file.